Das Programm wird �ber die Datei �main.m� gestartet. Es kann 
ausgew�hlt werden, wie viele Datens�tze auf einmal getestet werden. 
Durch setzen eines �%� vor den einzelnen Zeilen 
z.B. �result = [result edbv_ag_d3('Notenblatt0.PNG')];� wird dieses 
Notenblatt nicht getestet. Es ist m�glich alle Datens�tze auf einmal 
zu �berpr�fen und das Ergebnis wird in die Datei �result.txt� gespeichert. 
Weitere Parameter m�ssen nicht gesetzt werden. Bei Auswahl eines 
Datensatzes wird der Audio-Output in die Datei �result.wav� gespeichert 
und nach Abschluss der Verarbeitung abgespielt.
Nachdem das Programm gestartet wurde werden die Zeilen 2-21 abgearbeitet 
und nach Abschluss in die Datei �result.txt� gespeichert. 