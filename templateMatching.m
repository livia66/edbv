% searches in image I for the template T in the region given by ROI
% scales the template to the size of ROI and searches only inside of ROI
% this template matching is only partially scale invariant: it will scale
% down the template at max 10 times by the factor 0.95, to save computation
% time
% @param I images that will be searched
% @param T template image
% @param ROI region of interest, 2x2 matrix [startX startY; endX endY]
% @return maxValue maximal matching value found in I
% @return locR the x coord of the point with maxValue 
% @return locC the y coord of the point with maxValue

function [maxValue, locR, locC] = templateMatching(I, T, ROI)

    %level = graythresh(I);
    %I = im2bw(I,level);

    I = double(rgb2gray(I));      
    T = double(rgb2gray(T));

    sizeT = size(T);
    sizeI = size(I);
    
    if nargin == 2
        ROI = [1 1; sizeI(1) sizeI(2)]; 
    else
        ROI(1, 1) = max(ROI(1, 1), 1);
        ROI(1, 2) = max(ROI(1, 2), 1);
        ROI(2, 1) = min(ROI(2, 1), sizeI(1));
        ROI(2, 2) = min(ROI(2, 2), sizeI(2));
    end
    
    iRatio = 1;
    
    while sizeI(1) > 300
        I = imresize(I, 0.5);
        iRatio = iRatio * 0.5;
        ROI = ROI * 0.5;                    
        sizeI = size(I);
    end
    
    ROI = ceil(ROI);
    T = imresize(T, ((ROI(2, 1) - ROI(1, 1)) / (sizeT(1))) - 0.05);
    threshold = 0.8;
    [value, r, c] = templateMatchingFixImage(I, T, ROI);
    maxValue = value;
    locR = r;
    locC = c;
    
    count = 1;
    
    while value < threshold && count < 7
        
        T = imresize(T, 0.95);        
        [value, r, c] = templateMatchingFixImage(I, T, ROI);

        if maxValue < value
            maxValue = value;
            locR = r;
            locC = c;
        end
        
        count = count + 1;
    end
    
    locR = locR / iRatio;
    locC = locC / iRatio;
    
    I = imresize(I, 1.0/iRatio);
    I(locR-10:locR+10, locC-10:locC+10) = 255; 
    I = uint8(I);
    figure, imshow(I);

end

function [value, r, c] = templateMatchingFixImage(I, T, ROI)

    sizeI = size(I);
    sizeT = size(T);  
    A = ones(sizeI(1), sizeI(2)) * -1;   
    
    cx = floor(sizeT(1) / 2);
    cy = floor(sizeT(2) / 2);
    
    x = ROI(1, 1);
    y = ROI(1, 2);
    x2 = ROI(2, 1);
    y2 = ROI(2, 2);
    
    k = sizeT(1) * sizeT(2);
    sumT = sum(T(:));
    sumTSquare = T .^2;
    sumTSquare = sum(sumTSquare(:));
    MeanT = (1/k) * sumT;
    St = sqrt(sumTSquare - (k * (MeanT ^2)));
    
    for sx = x : x2 - sizeT(1)
        for sy = y : y2 - sizeT(2)
            hx = sx+sizeT(1)-1;
            hy = sy+sizeT(2)-1;
            SubMatrix = I(sx:hx, sy:hy); 
            
            sumSubMatrix = sum(SubMatrix(:));
            sumSubMatrixSquare = SubMatrix .^2;
            sumSubMatrixSquare = sum(sumSubMatrixSquare(:));
            sumCoefficient = (SubMatrix .* T);
            sumCoefficient = sum(sumCoefficient(:));
            
            MeanI = (1/k) * sumSubMatrix;
            Dividend = sumCoefficient - k * MeanI * MeanT;
            Divisor = 1 + sqrt(sumSubMatrixSquare - (k * (MeanI ^ 2))) * St;
            A(round(sx + cx), round(sy + cy)) = Dividend / Divisor;
        end
    end
    
    A = A + 1;
    A = A / 2;
    
    [value, location] = max(A(:));
     [r,c] = ind2sub(size(A),location); 
%     display(r);
%     display(c);
%     I(r:r+1, c:c+1) = 0; 
%     leftX = r-(sizeT(1)/2-1);
%     leftY = r+(sizeT(1)/2-1);
%     I(leftX : leftX+sizeT(1), leftY:leftY+sizeT(2)) = 0;
%     I = uint8(I);
%     figure, imshow(I);
end
