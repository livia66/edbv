function [ result ] = edbv_ag_d3(filename)
%% Notenerkennung
% Ziel ist es, nicht-handgeschriebene Musiknoten (Viertelnoten) zu erkennen,
% abzuspielen und diese textuell auszugeben.
% Es werden computergenerierte Noten in einem Bildformat  (eingescannt,
% abfotografiert, etc.) eingelesen und verarbeitet. Weiters ist keine
% zus�tzliche Eingabe n�tig.

% Gruppeninformation:
% Schiechl Kristina
% Wang Xi
% Ecker Livia Rose
% Widhalm Verena
% M�ller Marc

%Speichert die gefundenen y-Werte der Linien
LinesY = [];
%Speichert die gefundenen Y-Werte der Noten
NotesY = [];
%Speichert die berechneten y-Werte der Linien mit Zwischenraeumen
LinesYResult = [];
resultNotes = [];

%% Einlesen der Bilder
template_takt = imread('takt2.png');
template_cTakt = imread('cTakt.png');
template_bass = imread('bass.PNG');
template_schluessel = imread('notenschluessel.PNG');

namePicture = filename;
foto_notenblatt = imread(namePicture);

[rows columns numberOfColorChannels] = size(foto_notenblatt);
[rowsKey columnsKey numberOfColotChannels ] = size(template_schluessel);
[rowsTakt columnsTakt color] = size(template_takt);

% Wenn das Bild zu gro� ist, wird diese verkleinert.
% Es wird mit dem verkleinerten Bild weitergearbeitet.
if rows > 1000
    foto_notenblatt = imresize(foto_notenblatt, 1/3);
    [rows columns numberOfColorChannels] = size(foto_notenblatt);
end

%% Methodik
%% Schritt 1: Threshold nach Otsu
% Wird verwendet um ein Bin�rbild zu erhalten
resultOtsu = otsu(foto_notenblatt);

%% Schritt 2: Hough Transformation
% Wird verwendet um die Position der Linien zu erhalten.
% Wir ben�tigen die Linien um die Noten bestimmen zu k�nnen

%% Schritt 2a: Bildausschnitt erstellen
% Im ersten Schritt beschneiden wir das Bild und nehmen nur
% einen Teil aus dem Bild heraus. So gehen wir sicher, dass
% auch wirklich alle Linien gefunden werden
if columns/4 > 300
    % disp('columns/4 > 300');
    cropw = 300;
else
    %disp('columns');
    cropw = columns;
end
croppedPicHough = imcrop(resultOtsu, [columns/2 1 cropw rows]);
% figure,
% imshow(croppedPicHough);
% title('Beschnittenes Bild (fuer Hough)');

%% Schritt 2b: Region of Interest & Hough Transformation
% Zus�tzlich suchen wir uns die Region of Interest. Diese
% ben�tigen wir im n�chsten Schritt f�r das Template Matching
ROI = []; % Region of Interest.

[LinesYResult, ROI, abstandLinien] = houghTransformation(croppedPicHough);

%% Schritt 3: Template Matching:
%% Schritt 3a: Suchen des Noten- / Bassschluessels
% Wir suchen und den Noten- oder Bassschluessel im Bild. Je nachdem welcher
% der beiden vorhanden ist, werden die Noten benannt und ausgegeben.
% Als erstes suchen wir den Schluessel in unserem Bild, wenn dieser nicht
% vorhanden ist, dann suchen wir nach einem Bassschluessel. Wir speichern
% die Position des Schluessels.
schluessel = 'violin';
disp('suche nach key');
[maxValueKey, locR, locC ] = templateMatching(foto_notenblatt, template_schluessel, ROI);
%maxValueKey
%valueKey = maxValueKey;
if (maxValueKey <= 0.7)
    %kein Schluessel vorhaden, wir suchen nach dem Bassschluessel
    disp('suche nach bassschluessel');
    [maxValueKey, locR, locC] = templateMatching(foto_notenblatt, template_bass, ROI);
    %valueBass = maxValueKey;
    schluessel= 'bass';
end
%schluessel
%% Schritt 3b: Takt im Bild suchen
% Diese Position wird benoetigt um alles vor dem Takt abzuschneiden
% F�r die weitere Verarbeitung des Bildes sollen nur noch die Noten &
% Notenlinien vorhanden sein

[maxValueTakt, locR, locC] = templateMatching(foto_notenblatt, template_takt, ROI);
%maxValueTakt
if (maxValueTakt >= 0.7) & (maxValueTakt <= 1) % 4/4-Takt gefunden
    disp('takt gefunden -> bild wird beschnitten');
    croppedPicture3 = imcrop(foto_notenblatt, [locC+20 1 columns rows]);
else % kein 4/4-Takt gefunden, suchen c-Takt
    disp('takt nicht gefunden -> suchen cTakt');
    [maxValueTakt, locR, locC] = templateMatching(foto_notenblatt, template_cTakt, ROI);
    %  maxValueTakt
    if (maxValueTakt >= 0.7) & (maxValueTakt <= 1) % cTakt-gefunden
        disp('c-takt gefunden -> bild wird beschnitten');
        croppedPicture3 = imcrop(foto_notenblatt, [locC+20 1 columns rows]);
    else %kein 4/4 & c-Takt gefunden.
        %Wird verwenden die Position des Schluessels und beschneiden das
        %Bild
        if (maxValueKey >= 0.7) & (maxValueKey <= 1)
            disp('kein takt vorhanden. schneiden beim schluessel ab');
            croppedPicture3 = imcrop(foto_notenblatt, [locC+20 1 columns rows]);
        else
            error('Kein Takt und kein Notenschluessel vorhanden -> Abbruch: Bild kann nicht verarbeitet werden');
        end
    end
end
% figure,
% imshow(croppedPicture3);
% title('beschnittenes Bild (ohne Takt & Schluessel)');

%% Schritt 4: Morphologische Operation (Erosion)
morphPic = morph(otsu(croppedPicture3), abstandLinien);

%% Schritt 5: CCL
% Wird verwendet um die Position der Noten zu bestimmen
NotesY = ccl(morphPic);
if isempty(NotesY)
    error('Keine Noten vorhanden. Bild kann nicht verarbeitet werden');
end

%% Schritt 6: Ausgabe der Noten
%% Schritt 6a: Textuelle Ausgabe der Noten
noteNames = [];
if(strcmp(schluessel, 'violin'))
    noteNames = printNotes(LinesYResult, NotesY, abstandLinien);
end
if(strcmp(schluessel, 'bass'))
    noteNames = printNotesBass(LinesYResult, NotesY, abstandLinien);
end
%noteNames

output = expectedOutput(namePicture);
%output;
% Das Ergebnis wird in die result.txt-Datei geschrieben
% file = fopen('result.txt','wt');
% formatSpec = '%s %d';
fd = [];
percent = [];
if length(noteNames) == length(output)
    fd = noteNames == output;
    I = find(fd == 1);
    percent = length(I)/length(noteNames)*100;
end

result = [];
for i = 1:size(noteNames)
    result = [result 'Notenblatt: '];
    result = [result namePicture];
    result = [result '\n'];
    result = [result 'erwarteter output: '];
    result = [result output];
    result = [result '\n'];
    result = [result '           output: '];
    result = [result noteNames(i,:)];
    result = [result '\n'];
    result = [result 'korrekt? '];
    if strcmp(noteNames, output)
        result = [result 'true'];
    else
        result = [result 'false'];
    end
    result = [result '\n'];
    result = [result 'prozent? '];
    result = [result int2str(percent)];
    result = [result '\n'];
    result = [result '\n'];
    
end


%% Schritt 6b: Die musikalische Ausgabe der Noten
play(noteNames);
end

function[output] = expectedOutput(pictureName)
%% Schritt 6a: Speichert den erwarteten output f�r jedes Notenblatt ab
% Es wird �berpr�ft welches Notenblatt eingelesen wurde und anschlie�end
% wird der erwartete output abgespeichert und zur�ck gegeben

% Parameter:
% pictureName ... Name des Bildes das eingelesen wurde

% R�ckgabewert
% output ... der erwartete output f�r dieses Bild

if strcmp(pictureName, 'Notenblatt1.PNG')
    output = 'facacaEfchfDcDgfDad';
elseif strcmp(pictureName, 'Notenblatt0.PNG')
    output = 'cdef';
elseif strcmp(pictureName, 'Notenblatt2.PNG')
    output = 'efgahcDE';
elseif strcmp(pictureName, 'Notenblatt3.PNG')
    output = 'gaaaafeEhEgacfh';
elseif strcmp(pictureName, 'Notenblatt4.PNG')
    output = 'no notes';
elseif strcmp(pictureName, 'Notenblatt5.PNG')
    output = 'cacaagcahh';
elseif strcmp(pictureName, 'Notenblatt6.PNG')
    output = 'EccEDDaa';
elseif strcmp(pictureName, 'Notenblatt7.PNG')
    output = 'EEEEEEEEEE';
elseif strcmp(pictureName, 'Notenblatt8.PNG')
    output = 'hghgdhgdhghgdh';
elseif strcmp(pictureName, 'Notenblatt12.PNG')
    output = 'gccggEEccFEDchagg';
elseif strcmp(pictureName, 'Notenblatt13.PNG')
    output = 'GAH';
elseif strcmp(pictureName, 'Notenblatt14.PNG')
    output = 'hhhh';
elseif strcmp(pictureName, 'Notenblatt16.PNG')
    output = 'dggggf';
elseif strcmp(pictureName, 'Notenblatt17.PNG')
    output = 'fdgahagdefgedefg';
elseif strcmp(pictureName, 'Notenblatt18.PNG')
    output = 'ghagcchaffedeed';
elseif strcmp(pictureName, 'Notenblatt19.PNG')
    output = 'feeededceg';
elseif strcmp(pictureName, 'Notenblatt9.PNG')
    output = 'eefggfedccde';
elseif strcmp(pictureName, 'Notenblatt10.PNG')
    output = 'aceGghdF';
elseif strcmp(pictureName, 'Notenblatt11.PNG')
    output = 'efge';
elseif strcmp(pictureName, 'Notenblatt15.PNG')
    output = 'aDfEDhDf';
end

end

function[ binaryImg ] = otsu(image)
%% Schritt 1: Threshold nach Otsu
% Wir erzeugen ein Binaerbild mit der Methode "Threshold nach Otsu"
max = 0;
threshold = 0;

% grayscale image: RGB Bildpunkte werden in L-Grauwerte unterteilt
% Grauwerte: [1,2,...,L]
[rows columns numberOfColorChannels] = size(image);
if numberOfColorChannels > 1
    grayimg = rgb2gray(image);
else
    grayimg = image;
end

% histogram: Gibt die Anzahl der Bildpunkte pro Grauwert an. Die
% histogram(i) = ni .... Bildpunkte bei Level i
% Gesamtanzahl der Bildpunkte: N = n1+n2+...nL = sum(histogram)
histogram = imhist(grayimg);

% Wahrscheinlichkeitsverteilung: pi = ni/N    [N...Anzahl der Bildpunkte]
p = histogram/sum(histogram);

% Grenzwert wird eruiert
for n = 1:256   % ueber alle Grauwerte iterieren
    % Wahrscheinlichkeit der einzelnen Klassen: pC0 + pC1 = 1
    pC0 = sum(p(1:n));
    pC1 = 1-pC0;
    
    if(pC0 == 0 || pC1 == 0)
        continue;
    end
    
    % Mittelwert je Klasse: my0 = sum(i*pi/pC0)
    myC0 = sum((1:n)*p(1:n))/pC0;
    myC1 = sum((n+1:256)*p(n+1:256))/pC1;
    
    % Varianz innerhalb und zwischen den Klassen:
    % sigma = pC0*sigma0 + pC1*sigma1
    sigmaC0 = sum((((1:n)-myC0).^2)*p(1:n))/pC0;
    sigmaC1 = sum((((n+1:256)-myC1).^2)*p(n+1:256))/pC1;
    
    % sigma_between/sigma_within = max_value
    % wird benoetigt um den maximalen threshold zu errechnen
    sigmaW = pC0*sigmaC0 + pC1*sigmaC1;
    sigmaB = pC0*pC1*(myC1-myC0)*(myC1-myC0);
    
    sigmaT = sigmaB/sigmaW;
    
    if(sigmaT >= max)
        max = sigmaT;
        threshold = n;
    end
end

% Klassen werden im Bild neu gesetzt sodass ein Binaerbild entsteht
grayimg(grayimg <= threshold) = 0;
grayimg(grayimg > threshold) = 255;

binaryImg = grayimg;

% figure,
% imshow(binaryImg);
% title('Threshold nach Otsu');
end

function [ LinesYResult , ROI, abstandLinien] = houghTransformation(picture)
%% Schritt 2b: Hough Transformation
% Wir verwenden die Hough Transformation um die Linien am Notenblatt zu
% erkennen

% Schritt 2b.1: Kantendetektion (Sobel)
% Schritt 2b.2: Hough-Transformation finden
% Schritt 2b.3: Hough-Peaks finden
% Schritt 2b.4: Hough-Lines finden
% Schritt 2b.5: Speichern der x- & y-Werte der Notenlinien
% Schritt 2b.6: Berechnen des Abstandes & die zus�tzlichen Linien abspeichern
% Schritt 2b.7: Region of Interest abspeichern

% Parameter:
% picture ... das Bild auf dem die Operation angewendet wird

% Rueckgabe:
% LinesYResult ... die gesamten Y-Werte der Linien
% ROI ... Region of Interest.
% abstandLinien ... der Abstand der einzelnen Notenlinien

% Hilfreiche Links:
% https://de.mathworks.com/help/images/ref/hough.html
% https://de.mathworks.com/help/images/hough-transform.html#buh9ylp-26
% http://www.stackoverflow.com/questions/9310543/whats-the-use-of-canny-before-houghlines-opencv
% https://de.mathworks.com/help/images/edge-detection.html;

%% Schritt 2b.1: Kantendetektion mit Sobel
resultEdge = customSobel(picture);
% figure,
% imshow(resultEdge);
% title('Sobel Operator (Kantendetektion)');

%% Schritt 2b.2: Hough-Transformation und diese Anzeigen lassen
[H,theta,rho] = customHough(resultEdge);

%Die Hough-Transformation wird angezeigt
% figure,
% imshow(imadjust(mat2gray(H)),[],...
%     'XData',theta,...
%     'YData',rho,...
%     'InitialMagnification','fit');
% title('Hough Transformation')
% %Beschriftung der Achsen
% xlabel('\theta (degrees)')
% ylabel('\rho')
% axis on
% axis normal
% hold on
% colormap(hot)

%% Schritt 2b.3: Peak berechnen und diese Ausgeben lassen.
% Wir berechnen uns die Peaks
peak = customHoughPeaks(H,5,0.5*max(H(:)),ceil(0.3*max(H(:))));

% Ausgabe der Peaks
% x = theta(peak(:,2));
% y = rho(peak(:,1));
% plot(x,y,'s','color','black');
% title('Hough-Peaks');

%% Schritt 2b.4: Hough-Lines
% Linien mit Hilfe der Houghlines-Funktion finden und diese ausgeben lassen.
lines = customHoughLines(resultEdge, theta, rho, peak);

yPoints = []; %zum speichern der y-Werte der Notenlinien
xPoints = []; %zum speichern der x-Werte der Notenlinien

% Wir erzeugen einen plot und gibt diesen aus. Das Originalbild wird
% ausgegeben und zusaetzlich werden die Linien im Bild markiert
% figure,
% imshow(picture),
% title('Hough-Lines'),
% hold on
for k = 1:length(lines)
    xy = [lines(k).point1; lines(k).point2];
%     plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
    
    yPoints = [yPoints lines(k).point2(2)]; %wir speichern die y-Werte der Notenlinien
    xPoints = [xPoints lines(k).point1(2)]; %wir speichern die x-Werte der Notenlinien
    
    % Anfangs und Endpunkte werden markiert
%     plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
%     plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');
end

%% Schritt 2b.5: Speichern der x- & y-Werte
% Speichern der y- & x-Wert der Linien in einem Array und entfernen Duplikate.
linesYPoints = unique(yPoints);
linesXPoints = unique(xPoints);

%% Schritt 2b.6: Berechnen des Abstandes & die zus�tzlichen Linien abspeichern
% Wir speichern uns den Abstand der Linien ab
abstandextraLinien = abs(linesYPoints(2)-linesYPoints(1));

% Um auch die Noten ueber und unter den Linien zu erhalten, muessen wir uns
% die zus�tzlichen Positionen der Linien berechnen und diese zu den 5
% y-Werten der Linien speichern
extralinie1 = linesYPoints(1)- abstandextraLinien;
extralinie2 = linesYPoints(end) + abstandextraLinien;
linesYPoints = cat(2, extralinie1, linesYPoints, extralinie2);
abstandLinien=[];
for i=1:(length(linesYPoints)-1)
    abstandLinien = [abstandLinien linesYPoints(i+1)-linesYPoints(i)];
end
%abstandLinien
%% Schritt 2b.7: Region of Interest
% Wir speichern uns die Position der Linien am Eingabebild ab.
% Dies Position benoetigen wir fuer das Template Matching. Wir suchen nach
% dem Template in diesem Bereich
ROI = [linesXPoints(1) linesYPoints(1)-2*abstandLinien; linesXPoints(end) linesYPoints(end)+2*abstandLinien];

LinesY = linesYPoints;
LinesYResult = [LinesY(1) LinesY(1)+((LinesY(2)-LinesY(1))/2) LinesY(2) LinesY(2)+((LinesY(3)-LinesY(2))/2) LinesY(3) LinesY(3)+((LinesY(4)-LinesY(3))/2) LinesY(4) LinesY(4)+((LinesY(5)-LinesY(4))/2) LinesY(5) LinesY(5)+((LinesY(6)-LinesY(5))/2) LinesY(6) LinesY(6)+((LinesY(7)-LinesY(6))/2) LinesY(7)];

end

function [H, theta, rho] = customHough(image)
%% Schritt 2b.2: Houghberechnung
% Transformation der Eingabedaten in den Houghraum. [= rho und theta Koordinaten]
% Jede Gerade kann durch einen Punkt im (rho, theta)-Raum dargestellt
% werden. Theta liegt im Intervall [-90, +89] und rho darf negativ werden.
% Ein Punkt im Eingaberaum wird im Parameterraum durch eine Sinuskurve
% abgebildet.

% input:
% image... bin�res Kantenbild mit den zu detektirenden Geraden

% output:
% H ... Histogramm, Ergebnis der Hough-Transformation
% theta ... Theta Koordinate im Intervall [-90, +89]
% rho ... Rho Koordinate im Intervall -/+ maximaler Abstand vom Nullpunkt

[height, width] = size(image);

% maximaler Abstand vom Nullpunkt
% -1 da Endpunkt minus Startpunkt: (xb - xa)^2
max_d = round(sqrt((height-1)^2 + (width-1)^2));
min_d = max_d * -1;

theta = (-90:1:89);
rho = (min_d:1:max_d);
H = zeros(size(rho,2),size(theta,2));

% Abtastung aller Bildpunkte, wird f�r jeden Winkel mit Hilfe
% der HNV der Abstand zum Nullpunkt errechnet und die dazugeh�rige
% Arraystelle (Winkel und Abstand) inkrementiert.
for x=1:width
    for y=1:height
        if image(y,x)==255
            for angle=1:size(theta,2)
                r = fix(x*cosd(theta(angle)) + y*sind(theta(angle)))+1;
                H(find(rho==r),angle) = H(find(rho==r),angle) + 1;
            end
        end
    end
end
end

function lines = customHoughLines(E, theta, rho, peaks)

lines = repmat(struct('point1', [0, 0], 'point2', [0, 0], 'theta', 0, 'rho', 0), length(peaks), 1 );
sizeE = size(E);
for i = 1 : length(peaks)
    currentTheta = double(theta(round(peaks(i,2))));
    currentRho = double(rho(round(peaks(i,1))));
    r1 = (currentRho-double(cosd(currentTheta)))/double(sind(currentTheta));
    r2 = (currentRho-double(sizeE(2)*double(cosd(currentTheta))))/double(sind(currentTheta));
    point1 = [1, r1+1];
    point2 = [sizeE(2), r2+1];
    lines(i) = struct('point1', point1, 'point2', point2, 'theta', currentTheta, 'rho', currentRho);
end

end

function [maxValue, locR, locC] = templateMatching(I, T, ROI)
% searches in image I for the template T in the region given by ROI
% scales the template to the size of ROI and searches only inside of ROI
% this template matching is only partially scale invariant: it will scale
% down the template at max 10 times by the factor 0.95, to save computation
% time
% @param I images that will be searched
% @param T template image
% @param ROI region of interest, 2x2 matrix [startX startY; endX endY]
% @return maxValue maximal matching value found in I
% @return locR the x coord of the point with maxValue
% @return locC the y coord of the point with maxValue
%level = graythresh(I);
%I = im2bw(I,level);

I = double(rgb2gray(I));
T = double(rgb2gray(T));

% figure,
% subplot(2,1,1)
% imshow(I);
% title('Graustufenbild - Notenblatt');
% 
% subplot(2,1,2)
% imshow(T);
% title('Graustufenbild - Template');

sizeT = size(T);
sizeI = size(I);

if nargin == 2
    ROI = [1 1; sizeI(1) sizeI(2)];
else
    ROI(1, 1) = max(ROI(1, 1), 1);
    ROI(1, 2) = max(ROI(1, 2), 1);
    ROI(2, 1) = min(ROI(2, 1), sizeI(1));
    ROI(2, 2) = min(ROI(2, 2), sizeI(2));
end

iRatio = 1;

while sizeI(1) > 300
    I = imresize(I, 0.5);
    iRatio = iRatio * 0.5;
    ROI = ROI * 0.5;
    sizeI = size(I);
end

ROI = ceil(ROI);
T = imresize(T, ((ROI(2, 1) - ROI(1, 1)) / (sizeT(1))) - 0.05);
threshold = 0.8;
[value, r, c] = templateMatchingFixImage(I, T, ROI);
maxValue = value;
locR = r;
locC = c;

count = 1;

while value < threshold && count < 7
    sizeT = size(T);
    if ~(sizeT(1) < 5)
        T = imresize(T, 0.95);
    end
    [value, r, c] = templateMatchingFixImage(I, T, ROI);
    
    if maxValue < value
        maxValue = value;
        locR = r;
        locC = c;
    end
    
    count = count + 1;
end

locR = locR / iRatio;
locC = locC / iRatio;

%I = imresize(I, 1.0/iRatio);
%I(locR-10:locR+10, locC-10:locC+10) = 255;
%I = uint8(I);
%figure, imshow(I);

end

function [value, r, c] = templateMatchingFixImage(I, T, ROI)

sizeI = size(I);
sizeT = size(T);
A = ones(sizeI(1), sizeI(2)) * -1;

cx = floor(sizeT(1) / 2);
cy = floor(sizeT(2) / 2);

x = ROI(1, 1);
y = ROI(1, 2);
x2 = ROI(2, 1);
y2 = ROI(2, 2);

k = sizeT(1) * sizeT(2);
sumT = sum(T(:));
sumTSquare = T .^2;
sumTSquare = sum(sumTSquare(:));
MeanT = (1/k) * sumT;
St = sqrt(sumTSquare - (k * (MeanT ^2)));

for sx = x : x2 - sizeT(1)
    for sy = y : y2 - sizeT(2)
        hx = sx+sizeT(1)-1;
        hy = sy+sizeT(2)-1;
        SubMatrix = I(sx:hx, sy:hy);
        
        sumSubMatrix = sum(SubMatrix(:));
        sumSubMatrixSquare = SubMatrix .^2;
        sumSubMatrixSquare = sum(sumSubMatrixSquare(:));
        sumCoefficient = (SubMatrix .* T);
        sumCoefficient = sum(sumCoefficient(:));
        
        MeanI = (1/k) * sumSubMatrix;
        Dividend = sumCoefficient - k * MeanI * MeanT;
        Divisor = 1 + sqrt(sumSubMatrixSquare - (k * (MeanI ^ 2))) * St;
        A(round(sx + cx), round(sy + cy)) = Dividend / Divisor;
    end
end

A = A + 1;
A = A / 2;

[value, location] = max(A(:));
[r,c] = ind2sub(size(A),location);
end

function [ eroded ] = morph(picture, abstandLinien)
%% Schritt 4: Morphologische Operation - Erosion
% Die morphologische Operation (Erosion) wird verwendet, um die Linien
% im Bild zu entfernen (Notenlinien & Notenhaelse)

% Parameter:
% picture... das Binaerbild auf dem die Operation angewendet wird
% abstandLinien ... der Abstand der Linien. Wird benoetigt um die korrekte
%                   Groesse des Strukturelements zu berechnen

% Rueckgabe:
% eroded ... Dieses Bild enthaelt keine Notenlinien und Notenhaelse mehr

BW = ~im2bw(picture, 0.5); % wir tauschen schwarz mit wei�
se = strel('square', round(min(abstandLinien)*0.6)); %Strukturelement
eroded = imerode(BW, se); %Anwenden der Erosion

% figure,
% imshow(eroded);
% title('Erosion - Notenlinien entfernen');
end

function [ resultY ] = ccl(picture)
%% Schritt 5: Connected Component Labeling (CCL)
% Das Connected Component Labeling (CCL) wird verwendet, um die
% zusammengehoerigen Komponenten zu finden und so heraus zu finden welche
% Noten vorhanden sind.

% Parameter:
% picture... Bild auf dem die Operation angewendet wird

% Rueckgabe:
% resultY ... Die y-Position der Noten

% Hilfreiche Links:
% http://stackoverflow.com/questions/9067858/labeling-image-with-different-colors
% https://de.mathworks.com/help/images/labeling-and-measuring-objects-in-a-binary-image.html

%bwconncomp: Gibt die verbundenen Komponenten zurueck.
C = bwconncomp(picture);

%labelmatrix: Gibt Matrizen mit Komponenten zurueck
L = labelmatrix(C);
% figure,
% imagesc(L);
% title('CCL');

%Wir suchen uns nun die Koordinaten der Noten (Mitte des Notenkopfs)
maximum = max(L(:)); %Holen uns das Maximum aus der Labelmatrix heraus
resultY = []; %Enthaelt die y-Position der Noten.
for i = 1:maximum
    [x,y] = ind2sub(size(L),find(L==i));
    newy= (max(x)+min(x))/2;
    resultY = cat(1, resultY, newy);
end
end

function [result] = printNotesBass(LinesYResult, NotesY, abstandLinien)
%% Schritt 6a: Ausgabe der Noten (Basschluessel)
% In dieser Methode wird die Position der Linien und die Position der
% Noten verwendet um die Noten korrekt zuzuordnen und auszugeben

% Parameter
% LinesYResult ... die y-Position der Notenlinien
% NotesY ... die y-Position der Notenkoepfe
% abstandLinien ... der Abstand der Notenlinien

% Rueckgabe:
% result ... Die Noten auf dem Notenblatt

NoteNames = ['D' 'C' 'H' 'A' 'G' 'F' 'e' 'd' 'c' 'h' 'a' 'g' 'f' 'e' 'd'];

%length(LinesYResult)
result = [];
for k=1:length(NotesY)
    multiplikant = 3.95;
    
    if NotesY(k) < LinesYResult(1)-((abstandLinien(1)/multiplikant)*2)
        result = cat(2, result, NoteNames(1));
    end
    if NotesY(k) < LinesYResult(1)+(abstandLinien(1)/multiplikant) && NotesY(k)> LinesYResult(1)-(abstandLinien(1)/multiplikant)
        result = cat(2, result, NoteNames(2));
    end
    if NotesY(k) < LinesYResult(2)+(abstandLinien(1)/multiplikant) && NotesY(k)> LinesYResult(2)-(abstandLinien(1)/multiplikant)
        result = cat(2, result, NoteNames(3));
    end
    if NotesY(k) < LinesYResult(3)+(abstandLinien(2)/multiplikant) && NotesY(k)> LinesYResult(3)-(abstandLinien(2)/multiplikant)
        result = cat(2, result, NoteNames(4));
    end
    if NotesY(k) < LinesYResult(4)+(abstandLinien(2)/multiplikant) && NotesY(k)> LinesYResult(4)-(abstandLinien(2)/multiplikant)
        result = cat(2, result, NoteNames(5));
    end
    if NotesY(k) < LinesYResult(5)+(abstandLinien(3)/multiplikant) && NotesY(k)> LinesYResult(5)-(abstandLinien(3)/multiplikant)
        result = cat(2, result, NoteNames(6));
    end
    if NotesY(k) < LinesYResult(6)+(abstandLinien(3)/multiplikant) && NotesY(k)> LinesYResult(6)-(abstandLinien(3)/multiplikant)
        result = cat(2, result, NoteNames(7));
    end
    if NotesY(k) < LinesYResult(7)+(abstandLinien(4)/multiplikant) && NotesY(k)> LinesYResult(7)-(abstandLinien(4)/multiplikant)
        result = cat(2, result, NoteNames(8));
    end
    if NotesY(k) < LinesYResult(8)+(abstandLinien(4)/multiplikant) && NotesY(k)> LinesYResult(8)-(abstandLinien(4)/multiplikant)
        result = cat(2, result, NoteNames(9));
    end
    if NotesY(k) < LinesYResult(9)+(abstandLinien(5)/multiplikant) && NotesY(k)> LinesYResult(9)-(abstandLinien(5)/multiplikant)
        result = cat(2, result, NoteNames(10));
    end
    if NotesY(k) < LinesYResult(10)+(abstandLinien(5)/multiplikant) && NotesY(k)> LinesYResult(10)-(abstandLinien(5)/multiplikant)
        result = cat(2, result, NoteNames(11));
    end
    if NotesY(k) < LinesYResult(11)+(abstandLinien(6)/multiplikant) && NotesY(k)> LinesYResult(11)-(abstandLinien(6)/multiplikant)
        result = cat(2, result, NoteNames(12));
    end
    if NotesY(k) < LinesYResult(12)+(abstandLinien(6)/multiplikant) && NotesY(k)> LinesYResult(12)-(abstandLinien(6)/multiplikant)
        result = cat(2, result, NoteNames(13));
    end
    if NotesY(k) < LinesYResult(13)+(abstandLinien(6)/multiplikant) && NotesY(k)> LinesYResult(13)-(abstandLinien(6)/multiplikant)
        result = cat(2, result, NoteNames(14));
    end
    if NotesY(k) > (LinesYResult(13)+(abstandLinien(6)/multiplikant)*2)
        result = cat(2, result, NoteNames(15));
    end
end

end

function [result] = printNotes(LinesYResult, NotesY,abstandLinien)
%% Schritt 6a: Ausgabe der Noten (Notenschluessel)
% In dieser Methode wird die Position der Linien und die Position der
% Noten verwendet um die Noten korrekt zuzuordnen und auszugeben

% Parameter
% LinesYResult ... die y-Position der Notenlinien
% NotesY ... die y-Position der Notenkoepfe
% abstandLinien ... der Abstand der Notenlinien

% Rueckgabe:
% result ... Die Noten auf dem Notenblatt

NoteNames = ['H' 'A' 'G' 'F' 'E' 'D' 'c' 'h' 'a' 'g' 'f' 'e' 'd' 'c' 'h'];
%abstandLinien
%length(LinesYResult)
result = [];
for k=1:length(NotesY)
    multiplikant = 3.95;
    
    if NotesY(k) < LinesYResult(1)-((abstandLinien(1)/multiplikant)*2)
        result = cat(2, result, NoteNames(1));
    end
    if NotesY(k) < LinesYResult(1)+(abstandLinien(1)/multiplikant) && NotesY(k)> LinesYResult(1)-(abstandLinien(1)/multiplikant)
        result = cat(2, result, NoteNames(2));
    end
    if NotesY(k) < LinesYResult(2)+(abstandLinien(1)/multiplikant) && NotesY(k)> LinesYResult(2)-(abstandLinien(1)/multiplikant)
        result = cat(2, result, NoteNames(3));
    end
    if NotesY(k) < LinesYResult(3)+(abstandLinien(2)/multiplikant) && NotesY(k)> LinesYResult(3)-(abstandLinien(2)/multiplikant)
        result = cat(2, result, NoteNames(4));
    end
    if NotesY(k) < LinesYResult(4)+(abstandLinien(2)/multiplikant) && NotesY(k)> LinesYResult(4)-(abstandLinien(2)/multiplikant)
        result = cat(2, result, NoteNames(5));
    end
    if NotesY(k) < LinesYResult(5)+(abstandLinien(3)/multiplikant) && NotesY(k)> LinesYResult(5)-(abstandLinien(3)/multiplikant)
        result = cat(2, result, NoteNames(6));
    end
    if NotesY(k) < LinesYResult(6)+(abstandLinien(3)/multiplikant) && NotesY(k)> LinesYResult(6)-(abstandLinien(3)/multiplikant)
        result = cat(2, result, NoteNames(7));
    end
    if NotesY(k) < LinesYResult(7)+(abstandLinien(4)/multiplikant) && NotesY(k)> LinesYResult(7)-(abstandLinien(4)/multiplikant)
        result = cat(2, result, NoteNames(8));
    end
    if NotesY(k) < LinesYResult(8)+(abstandLinien(4)/multiplikant) && NotesY(k)> LinesYResult(8)-(abstandLinien(4)/multiplikant)
        result = cat(2, result, NoteNames(9));
    end
    if NotesY(k) < LinesYResult(9)+(abstandLinien(5)/multiplikant) && NotesY(k)> LinesYResult(9)-(abstandLinien(5)/multiplikant)
        result = cat(2, result, NoteNames(10));
    end
    if NotesY(k) < LinesYResult(10)+(abstandLinien(5)/multiplikant) && NotesY(k)> LinesYResult(10)-(abstandLinien(5)/multiplikant)
        result = cat(2, result, NoteNames(11));
    end
    if NotesY(k) < LinesYResult(11)+(abstandLinien(6)/multiplikant) && NotesY(k)> LinesYResult(11)-(abstandLinien(6)/multiplikant)
        result = cat(2, result, NoteNames(12));
    end
    if NotesY(k) < LinesYResult(12)+(abstandLinien(6)/multiplikant) && NotesY(k)> LinesYResult(12)-(abstandLinien(6)/multiplikant)
        result = cat(2, result, NoteNames(13));
    end
    if NotesY(k) < LinesYResult(13)+(abstandLinien(6)/multiplikant) && NotesY(k)> LinesYResult(13)-(abstandLinien(6)/multiplikant)
        result = cat(2, result, NoteNames(14));
    end
    if NotesY(k) > (LinesYResult(13)+(abstandLinien(6)/multiplikant)*2)
        result = cat(2, result, NoteNames(15));
    end
end

end

function peaks = customHoughPeaks(H, N, tresh, nhood)
%% Schritt 2b.3: Finden der Houghpeaks
% Lokalisieren der N maximalen Peaks aus der Hough-Transformationsmatrix H
% unter Ber�cksichtigung der Nachbarschaft nhood und des Schwellwerts
% tresh. Wir iterieren ueber das gesamte Bild und suchen Werte ueber dem
% Schwellwert, welche im potPeaks gespeichert werden. Hat man einen solchen
% gefunden, pruefen wir die Nachbarschaft rekursiv auf evtl noch groe�ere
% Werte. Findet man keine groe�eren Werte, so hat man lokal ein Maximum
% gefunden, wessen Nachbarschaft 0 gesetzt wird. Die N hoechsten Peaks
% werden schliesslich zurueck geliefert.

% input:
% H... Histogramm, Ergebnis der Hough-Transformation
% N... Anzahl der zu findenden peaks
% tresh... Schwellwert, der entscheidet, ob ein peak als peak erkannt wird
% hood... Groe�e der Nachbarschaft

% output:
% peaks... Nx2 Array mit den x/y-Koordinaten der N maximalen peaks

%potPeaks ist maximal so gro�, wie die Summe aller Zahlen in H > tresh
potPeaks = zeros(sum(sum(H>tresh)),3);
counter = 1;

% Berechnung des Nachbarschaftfeldes. Wird aufgerundet und, falls gerade,
% +1 addiert
newhood = ceil(sqrt(nhood));
if mod(newhood, 2) == 0
    newhood = newhood + 1;
end

% Iteration ueber H
for i=1:size(H,1)
    for j=1:size(H,2)
        % Ist ein Feld H(i,j) > tresh, so rufen wir checkNhood auf um in
        % der Nachbarschaft evtl groe�ere Werte zu finden.
        if H(i,j) > tresh
            % tmp(1) = x-Koord, tmp(2) = y-Koord, tmp(3) = Wert des Peaks
            [tmp(1),tmp(2),tmp(3)] = checkNhood(H,i,j,newhood);
            
            % Setzen der Nachbarschaft auf 0 unter Beruecksichtigung der
            % Raender der Matrix
            x1 = max(tmp(1)-fix(newhood/2),1);
            y1 = max(tmp(2)-fix(newhood/2),1);
            x2 = min(size(H,1),tmp(1)+fix(newhood/2));
            y2 = min(size(H,2),tmp(2)+fix(newhood/2));
            
            H(tmp(1),tmp(2)) = 0;
            
            [newi, newj] = get2ndPeak(H(x1:x2,y1:y2));
            newx = x1+newi-1;
            newy = y1+newj-1;
            
            tmpy = ceil(abs(tmp(1)-newx)/2);
            finaly = min(newx,tmp(1)) + tmpy;
            
            H(x1:x2,y1:y2) = 0;
            
            potPeaks(counter,:) = [finaly tmp(2) tmp(3)];
            counter = counter + 1;
        end
    end
end

% Die potenziellenPeaks werden sortiert, und die gr��ten N and peaks
% uebergeben
potPeaks = sortrows(potPeaks,3);
peaks = potPeaks(size(potPeaks,1)-N+1:size(potPeaks,1),1:2);
end

function [x,y,m] = checkNhood(H,i,j,newhood)
%% Schritt 2b.3: Ueberpruefen der Nachbarschaft auf groe�ere Werte
% Diese rekursive Hilfsmethode dient dazu, die Nachbarschaft eines Peaks auf
% weitere, hoehere Werte zu untersuchen. Auf diesem hoeherem Wert ruft sich
% checkNhood wiederum erneut auf, solange, bis wir keine hoeheren Peaks mehr
% finden. Die Koordinaten und der Wert des Maximums wird retuniert.

% input:
% H... Histogramm, Ergebnis der Hough-Transformation
% i... x-Koordinate des aktuellen Peaks
% j... x-Koordinate des aktuellen Peaks
% newhood... Groe�e der berechneten Kante einer Nachbarschaft
%            Nachbarschaft = newhood x newhood

% output:
% x... x-Koordinate des evtl neuen, groe�eren Peaks in der Nachbarschaft
% y... y-Koordinate des evtl neuen, groe�eren Peaks in der Nachbarschaft
% m... Wert des evtl neuen, groe�eren Peaks in der Nachbarschaft
%Passe die neighbourhood ensprechend der Randbereiche an
x1 = max(i-fix(newhood/2),1);
y1 = max(j-fix(newhood/2),1);
x2 = min(size(H,1),i+fix(newhood/2));
y2 = min(size(H,2),j+fix(newhood/2));
neighbourhood = H(x1:x2,y1:y2);

% Finde den maximalen Wert in der Neighbourhood
maxval = max(max(neighbourhood));

tmp = [i,j, maxval];

% Falls es einen Wert in der Nachbarschaft gibt, der groe�er ist als der
% aktuelle Wert, finde ihn und f�hre auf ihm rekursive checkNhood aus
if maxval > H(i,j)
    [row, col] = find(neighbourhood == maxval);
    [tmp(1),tmp(2),tmp(3)] = checkNhood(H,row(1)+x1-1,col(1)+y1-1,newhood);
end

x = tmp(1);
y = tmp(2);
m = tmp(3);
end


function [newi, newj] = get2ndPeak(nhood)
%% Schritt 2b.3: Finden des 2nd Peak der lokalen Max-Peak-Neighbourhood
%
maxval = max(max(nhood));
[row, col] = find(nhood == maxval);
newi = row(1);
newj = col(1);
end



function edges = customSobel(picture)
%% Schritt 2b.1: Kantendetection mittels Sobelfilter
%      1 0 -1       1  2  1   Aus dem bin�ren Eingabebild erzeugen wir, mit
% sx = 2 0 -2  sy = 0  0  0   mithilfe der beiden nebenstehenden
%      1 0 -1      -1 -2 -1   Sobelkernel, jeweils einmal in x-Richtung
%                             und einmal in y-Richtung, das gewuenschte
% Gradientenbild nach Pythagoras.

% input:
% picture... bin�res, beschnittenes Eingabebild

% output:
% edges... das fertige Gradientenbild
B=picture;
C=double(B);

for i=1:size(C,1)
    for j=1:size(C,2)
        %Mittelfeld
        if(i > 1 && i < size(C,1) && j > 1 && j < size(C,2))
            Gx = ((C(i+1,j-1)+2*C(i+1,j)+C(i+1,j+1))-(C(i-1,j-1)+2*C(i-1,j)+C(i-1,j+1)));
            Gy = ((C(i-1,j+1)+2*C(i,j+1)+C(i+1,j+1))-(C(i-1,j-1)+2*C(i,j-1)+C(i+1,j-1)));
            %Bildgradient
            B(i,j)=sqrt(Gx.^2+Gy.^2);
        end
    end
end

%Edge-Strategie: Bildrand ensprechend erweitern
for i=1:size(C,1)
    B(i,1)=B(i,2);
    B(i,size(C,2))= B(i,size(C,2)-1);
end

for j=1:size(C,2)
    B(1,j)=B(2,j);
    B(size(C,1),j)= B(size(C,1)-1,j);
end

edges = B;
end

function [ ausgabe ] = play(noteNames)
%% Schritt 6b: Musikalische Ausgabe der Noten.
% Die Noten werden in Frequenzen abgespeichert und es wird ueberprueft
% welche Noten eingeben werden.
% Diese Noten werden in eine Audio-Datei geschrieben und anschliessend
% abgespielt

% Parameter:
% noteNames ... die gefundenen Noten am Notenblatt
% Umwandeln der Noten

H=sin(2*pi*493.88*(0:0.000125:0.5));
A=sin(2*pi*440*(0:0.000125:0.5));
G=sin(2*pi*391.99*(0:0.000125:0.5));
F=sin(2*pi*349.22*(0:0.000125:0.5));
E=sin(2*pi*329.62*(0:0.000125:0.5));
D=sin(2*pi*293.66*(0:0.000125:0.5));
c=sin(2*pi*261.6*(0:0.000125:0.5));
%Frequenz f�r B weil ich die f�r H noch nicht gefunden habe
h=sin(2*pi*246.94*(0:0.000125:0.5));
a=sin(2*pi*220*(0:0.000125:0.5));
g=sin(2*pi*195.99*(0:0.000125:0.5));
f=sin(2*pi*174.61*(0:0.000125:0.5));
e=sin(2*pi*164.81*(0:0.000125:0.5));
d=sin(2*pi*146.83*(0:0.000125:0.5));
ausgabe = [];
%c''' h''' fehlt
for i=1:length(noteNames)
    if(strcmp(noteNames(i),'H'))
        ausgabe = [ausgabe H];
    elseif(strcmp(noteNames(i),'A'))
        ausgabe = [ausgabe A];
    elseif(strcmp(noteNames(i),'G'))
        ausgabe = [ausgabe G];
    elseif(strcmp(noteNames(i),'F'))
        ausgabe = [ausgabe F];
    elseif(strcmp(noteNames(i),'E'))
        ausgabe = [ausgabe E];
    elseif(strcmp(noteNames(i),'D'))
        ausgabe = [ausgabe D];
    elseif(strcmp(noteNames(i),'c'))
        ausgabe = [ausgabe c];
    elseif(strcmp(noteNames(i),'h'))
        ausgabe = [ausgabe h];
    elseif(strcmp(noteNames(i),'a'))
        ausgabe = [ausgabe a];
    elseif(strcmp(noteNames(i),'g'))
        ausgabe = [ausgabe g];
    elseif(strcmp(noteNames(i),'f'))
        ausgabe = [ausgabe f];
    elseif(strcmp(noteNames(i),'e'))
        ausgabe = [ausgabe e];
    elseif(strcmp(noteNames(i),'d'))
        ausgabe = [ausgabe d];
    else
    end
end

Fs=5100;
audiowrite('result.wav',ausgabe,Fs);
clear ausgabe Fs;
[ausgabe, Fs] = audioread('result.wav');
sound(ausgabe,Fs);
end