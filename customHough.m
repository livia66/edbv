%% Houghberechnung
% Transformation der Eingabedaten in den Houghraum. [rho und theta Koordinaten]
% Jede Gerade kann durch einen Punkt im (rho, theta)-Raum dargestellt
% werden. Theta liegt im Intervall [0, Pi) und rho darf negativ werden.
% Ein PUnkt im Eingaberaum wird im Parameterraum durch eine Sinuskurve
% abgebildet.

% input:
% image... bin�res Kantenbild mit den zu detektirenden Geraden

% output:
% H ... Histogramm, Ergebnis der Hough-Transformation
% theta ... 
% rho ... 
function [H, theta, rho] = customHough(image)

    [height, width] = size(image);
    
    % maximaler Abstand vom Nullpunkt
    % -1 da Endpunkt minus Startpunkt: (xb - xa)^2
    max_d = round(sqrt((height-1)^2 + (width-1)^2));
    min_d = max_d * -1;
    
    theta = (-90:1:89); % theta [-90,90] TODO: evtl. +90 noch entfernen 
    rho = (min_d:1:max_d); 
    H = zeros(size(rho,2),size(theta,2));

    % Abtastung aller Bildpunkte, wird f�r jeden Winkel mit Hilfe
    % der HNV der Abstand zum Nullpunkt errechnet und die dazugeh�rige
    % Arraystelle (Winkel und Abstand) inkrementiert.
    for x=1:width
       for y=1:height
           if image(y,x)==1
              for angle=1:size(theta,2)
                % rho = x*cos(theta) + y*sin(theta)
                r = round(x*cosd(theta(angle)) + y*sind(theta(angle)));
                H(find(rho==r),angle) = H(find(rho==r),angle) + 1;
              end
           end
       end
    end
end