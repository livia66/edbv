function [ ausgabe ] = play(noteNames)
%% Musikalische Ausgabe der Noten.
% Die Noten werden in Frequenzen abgespeichert und es wird ueberprueft
% welche Noten eingeben werden.
% Diese Noten werden in eine Audio-Datei geschrieben und anschliessend
% abgespielt

% Parameter:
% noteNames ... die gefundenen Noten am Notenblatt

% Umwandeln der Noten
H=sin(2*pi*493.88*(0:0.000125:0.5));
A=sin(2*pi*440*(0:0.000125:0.5));
G=sin(2*pi*391.99*(0:0.000125:0.5));
F=sin(2*pi*349.22*(0:0.000125:0.5));
E=sin(2*pi*329.62*(0:0.000125:0.5));
D=sin(2*pi*293.66*(0:0.000125:0.5));
c=sin(2*pi*261.6*(0:0.000125:0.5));
%Frequenz f�r B weil ich die f�r H noch nicht gefunden habe
h=sin(2*pi*246.94*(0:0.000125:0.5));
a=sin(2*pi*220*(0:0.000125:0.5));
g=sin(2*pi*195.99*(0:0.000125:0.5));
f=sin(2*pi*174.61*(0:0.000125:0.5));
e=sin(2*pi*164.81*(0:0.000125:0.5));
d=sin(2*pi*146.83*(0:0.000125:0.5));
ausgabe = [];
%c''' h''' fehlt

for i=1:length(noteNames)
    if(strcmp(noteNames(i),'H'))
        ausgabe = [ausgabe H];
    elseif(strcmp(noteNames(i),'A'))
        ausgabe = [ausgabe A];
    elseif(strcmp(noteNames(i),'G'))
        ausgabe = [ausgabe G];
    elseif(strcmp(noteNames(i),'F'))
        ausgabe = [ausgabe F];
    elseif(strcmp(noteNames(i),'E'))
        ausgabe = [ausgabe E];
    elseif(strcmp(noteNames(i),'D'))
        ausgabe = [ausgabe D];
    elseif(strcmp(noteNames(i),'c'))
        ausgabe = [ausgabe c];
    elseif(strcmp(noteNames(i),'h'))
        ausgabe = [ausgabe h];
    elseif(strcmp(noteNames(i),'a'))
        ausgabe = [ausgabe a];
    elseif(strcmp(noteNames(i),'g'))
        ausgabe = [ausgabe g];
    elseif(strcmp(noteNames(i),'f'))
        ausgabe = [ausgabe f];
    elseif(strcmp(noteNames(i),'e'))
        ausgabe = [ausgabe e];
    elseif(strcmp(noteNames(i),'d'))
        ausgabe = [ausgabe d];
    else
    end
end

Fs=5100;
audiowrite('result.wav',ausgabe,Fs);
clear ausgabe Fs;
[ausgabe, Fs] = audioread('result.wav');

sound(ausgabe,Fs);
end
