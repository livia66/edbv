%% Kantendetection mittels Sobelfilter
%      1 0 -1       1  2  1   Aus dem bin�ren Eingabebild erzeugen wir, mit
% sx = 2 0 -2  sy = 0  0  0   mithilfe der beiden nebenstehenden
%      1 0 -1      -1 -2 -1   Sobelkernel, jeweils einmal in x-Richtung
%                             und einmal in y-Richtung, das gewuenschte
% Gradientenbild nach Pythagoras.

% input:
% picture... bin�res, beschnittenes Eingabebild

% output:
% edges... das fertige Gradientenbild

function edges = customSobel(picture)
B=picture;
C=double(B);

%Edge-Strategie: Au�erhalb des Bildes auf fac (0 oder 255) setzen
fac = 255;

for i=1:size(C,1)
    for j=1:size(C,2)
        %Ecke links oben
        if(i == 1 && j == 1)
            Gx = (fac + 2*C(i+1,j)+C(i+1,j+1)) - 3*fac;
            Gy = (fac + 2*C(i,j+1)+C(i+1,j+1)) - 3*fac;
        end
        %Kante oben
        if(i == 1 && j > 1 && j < size(C,2))
            Gx = (C(i+1,j-1)+2*C(i+1,j)+C(i+1,j+1)) - 3*fac;
            Gy = (fac + 2*C(i,j+1)+C(i+1,j+1))-(fac + 2*C(i,j-1)+C(i+1,j-1));
        end
        %Ecke rechts oben
        if(i == 1 && j == size(C,2))
            Gx = (C(i+1,j-1)+2*C(i+1,j) + fac) - 3*fac;
            Gy = 3*fac - (fac + 2*C(i,j-1)+C(i+1,j-1));
        end
        %Ecke unten links
        if(i == size(C,1) && j == 1)
            Gx = 3*fac - (fac + 2*C(i-1,j)+C(i-1,j+1));
            Gy = (C(i-1,j+1)+2*C(i,j+1) + fac) - 3*fac;
        end
        %Kante unten
        if(i == size(C,1) && j > 1 && j < size(C,2))
            Gx = 3*fac - (C(i-1,j-1)+2*C(i-1,j)+C(i-1,j+1));
            Gy = ((C(i-1,j+1)+2*C(i,j+1) + fac)-(C(i-1,j-1)+2*C(i,j-1) + fac));
        end
        %Ecke unten rechts
        if(i == size(C,1) && j == size(C,2))
            Gx = 3*fac - (C(i-1,j-1)+2*C(i-1,j) + fac);
            Gy = 3*fac - (C(i-1,j-1)+2*C(i,j-1) + fac);
        end
        %Kante links
        if(j == 1 && i > 1 && i < size(C,1))
            Gx = (fac + 2*C(i+1,j)+C(i+1,j+1))-(fac + 2*C(i-1,j)+C(i-1,j+1));
            Gy = (C(i-1,j+1)+2*C(i,j+1)+C(i+1,j+1)) - 3*fac;
        end
        %Kante rechts
        if(j == size(C,2) && i > 1 && i < size(C,1))
            Gx = (C(i+1,j-1)+2*C(i+1,j) + fac) - (C(i-1,j-1)+2*C(i-1,j) + fac);
            Gy = 3*fac - (C(i-1,j-1)+2*C(i,j-1)+C(i+1,j-1));
        end
        %Mittelfeld
        if(i > 1 && i < size(C,1) && j > 1 && j < size(C,2))
            Gx = ((C(i+1,j-1)+2*C(i+1,j)+C(i+1,j+1))-(C(i-1,j-1)+2*C(i-1,j)+C(i-1,j+1)));       
            Gy = ((C(i-1,j+1)+2*C(i,j+1)+C(i+1,j+1))-(C(i-1,j-1)+2*C(i,j-1)+C(i+1,j-1)));
            %Bildgradient
            B(i,j)=sqrt(Gx.^2+Gy.^2);
        end
        
    end
end
edges = B;
end