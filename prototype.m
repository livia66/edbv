function [ binaryPicture, resultHoughTransformation, resultTemplateMatching ] = prototype()
%% Notenerkennung
% Ziel ist es, nicht-handgeschriebene Musiknoten (Viertelnoten) zu erkennen,
% abzuspielen und diese textuell auszugeben.
% Es werden computergenerierte Noten in einem Bildformat  (eingescannt,
% abfotografiert, etc.) eingelesen und verarbeitet. Weiters ist keine
% zus�tzliche Eingabe n�tig.

% Gruppeninformation:
% Schiechl Kristina
% Wang Xi
% Ecker Livia Rose
% Widhalm Verena
% M�ller Marc

%Speichert die gefundenen LinienY werte
LinesY = [];
%Speichert die gefundenen Noten Y werte
NotesY = [];
%Speichert die berechneten LinienY werte mit Zwischenraeumen
LinesYResult = [];

%% probleme dataset:

% 4 es ist kein takt im bild vorhanden. bild findet trotzdem ein template
% 12 hier ist der notenschluessel leicht beschnitten. er wird nicht
% gefunden
% 13 hier wird der basschluessel als takt erkannt
% 16 template wird nicht erkannt. erosion funktioniert nicht
% 18 linien werden nicht korrekt erkannt. fehler bei hough Transformation
% 19 template-matching nicht korrekt
% 21 probleme beim erkennen der korrekten pixel (hintergrund, noten +
% linien)
% 23 bild viel zu gross, braucht sehr lange-> ergebnis falsch

%% Generelle Probleme:

% Template Matching:
% Es wird ein Wert berechnet (Metric), der angibt, wie gleich sich das
% Template und der gefundene Bereich im Bild sind. Dieser Wert f�hrt aber
% zu falschen Ergebnissen. Es werden Templates erkannt, obwohl diese nicht
% im Bild vorhanden sind. Zur Zeit darf der Wert zwischen
% if (matchValueKey >= 0) & (matchValueKey <= 80) liegen um das Template
% als gefunden zu markieren.
% Wenn der Hintergrund nicht wei�, sondern grau ist, gibt es beim erkennen
% der einzelnen templates probleme z.B. 21 & 23

%% Einlesen der Bilder
template_takt = imread('takt2.png');
template_cTakt = imread('cTakt.png');
template_bass = imread('bass.PNG');
template_schluessel = imread('notenschluessel.PNG');

foto_notenblatt = imread('Notenblatt0.PNG');

[rows columns numberOfColorChannels] = size(foto_notenblatt);
[rowsKey columnsKey numberOfColotChannels ] = size(template_schluessel);
[rowsTakt columnsTakt color] = size(template_takt);

% Wenn das Bild zu gro� ist, wird diese verkleinert.
% Es wird mit dem verkleinerten Bild weitergearbeitet.
if rows > 1000
    foto_notenblatt = imresize(foto_notenblatt, 1/3);
    [rows columns numberOfColorChannels] = size(foto_notenblatt);
end

%% Methodik
%% Schritt 1: Threshold nach Otsu
% Wird verwendet um ein Bin�rbild zu erhalten
resultOtsu = otsu(foto_notenblatt);

%% Schritt 2: Hough Transformation
% Wird verwendet um die Position der Linien zu erhalten. Diese ben�tigen wir
% um die einzelnen Noten zu bestimmen.

%% Schritt 2a: Bildausschnitt erstellen
% Im ersten Schritt beschneiden wir das Bild und nehmen nur
% einen Teil aus der Mitte des Bildes heraus. So gehen wir sicher, dass
% auch wirklich alle Linien gefunden werden
if columns/4 > 300
    disp('columns/4 > 300');
    cropw = 300;
else
    disp('columns');
    cropw = columns;
end
croppedPicHough = imcrop(resultOtsu, [columns/2 1 cropw rows]);
figure,
imshow(croppedPicHough);
title('Beschnittenes Bild (fuer Hough)');

%% Schritt 2b: Region of Interest & Hough Transformation
% Zus�tzlich suchen wir uns die Region of Interest. Diese
% ben�tigen wir im n�chsten Schritt f�r das Template Matching
ROI = []; % Region of Interest.

[LinesYResult, ROI, abstandLinien] = houghTransformation(croppedPicHough);

%% Schritt 3: Template Matching:
%% Schritt 3a: Suchen des Noten- / Bassschluessels
% Wir suchen und den Noten- oder Bassschluessel im Bild. Je nachdem welcher
% der beiden vorhanden ist, werden die Noten benannt und ausgegeben.
% Als erstes suchen wir den Schluessel in unserem Bild, wenn dieser nicht
% vorhanden ist, dann suchen wir nach einem Bassschluessel. Wir speichern
% die Position des Schluessels.

disp('suche nach key');
[positionClef, matchValueKey] = templateMatching(foto_notenblatt, template_schluessel, ROI);
if (matchValueKey <= 0) || (matchValueKey >= 80)
     %kein Schlueesel vorhaden, wir suchen nach dem Bassschluessel
    disp('suche nach bassschluessel');
    [positionClef, matchValueKey] = templateMatching(foto_notenblatt, template_bass, ROI);
end

%% Schritt 3b: Takt im Bild suchen
% Diese Position wird benoetigt um alles vor dem Takt abzuschneiden 
% F�r die weitere Verarbeitung des Bildes sollen nur noch die Noten +
% Notenlinien vorhanden sein

[positionTakt, matchValueTakt] = templateMatching(foto_notenblatt, template_takt, ROI);

if (matchValueTakt >= 0) & (matchValueTakt <= 80) % 4/4-Takt gefunden
    disp('takt gefunden -> bild wird beschnitten');
    croppedPicture3 = imcrop(foto_notenblatt, [(positionTakt(1)+20) 1 columns rows]);
else % kein 4/4-Takt gefunden, suchen c-Takt
    [positionTakt, matchValueTakt] = templateMatching(foto_notenblatt, template_cTakt, ROI);
    %matchValueTakt
    if (matchValueTakt >= 0) & (matchValueTakt <= 80) % cTakt-gefunden
        disp('c-takt gefunden -> bild wird beschnitten');
        croppedPicture3 = imcrop(foto_notenblatt, [(positionTakt(1)+20) 1 columns rows]);
    else %kein 4/4 & c-Takt gefunden.
        %Wir verwenden die Position des Schluessels und beschneiden das
        %Bild
        if (matchValueKey >= 0) || (matchValueKey <= 80)
            disp('kein takt vorhanden. schneiden beim schluessel ab');
            croppedPicture3 = imcrop(foto_notenblatt, [(positionClef(1)+20) 1 columns rows]);
        else
            error('Kein Takt und kein Notenschluessel vorhanden -> Abbruch: Bild kann nicht verarbeitet werden');
        end
    end
end
figure,
imshow(croppedPicture3);
title('beschnittenes Bild (ohne Takt & Schluessel)');

%% Schritt 4: Morphologische Operation (Erosion)
morphPic = morph(croppedPicture3, abstandLinien);

%% Schritt 5: CCL
% Wir verwendet um die Position der Noten zu bestimmen
NotesY = ccl(morphPic);
NotesY
if isempty(NotesY)
    error('Keine Noten vorhanden. Bild kann nicht verarbeitet werden');
end

%% Schritt 6: Ausgabe der Noten
%% Schritt 6a: Textuelle Ausgabe der Noten
% TODO wir muessen hier die ausgabe der noten an den noten/basschluessel
% anpassen.
%printNotes(LinesYResult, NotesY);


% TODO vl eine ausgabe in einer datei mit output und erwartetet output
%% Schritt 6b: Die musikalische Ausgabe der Noten

end

function [] = printNotesBass(LinesYResult, NotesY)
%% Ausgabe der Noten (Basschluessel)
% In dieser Methode wird die Position der Linien und die Position der
% Noten verwendet um die Noten korrekt zuzuordnen und auszugeben

% Parameter
% LinesYResult ... die y-Position der Notenlinien
% NotesY ... die y-Position der Notenkoepfe

NoteNames = ['g' 'f' 'e' 'd' 'c' 'h' 'a' 'g' 'f' 'e' 'd'];

result = [];

for k=1:4
    for i=1:9
        if (LinesYResult(i) - NotesY(k))<=2 && LinesYResult(i) - NotesY(k)>=-2
            result = cat(2, result, NoteNames(i+1));
        end
    end
    if NotesY(k)> (LinesYResult(9)+2)
        result = cat(2, result, NoteNames(11));
    end
    if NotesY(k)+2 < LinesYResult(1)
        result = cat(2, result, NoteNames(1));
    end
end
result

end

function [] = printNotes(LinesYResult, NotesY)
%% Ausgabe der Noten (Notenschluessel)
% In dieser Methode wird die Position der Linien und die Position der
% Noten verwendet um die Noten korrekt zuzuordnen und auszugeben

% Parameter
% LinesYResult ... die y-Position der Notenlinien
% NotesY ... die y-Position der Notenkoepfe

% TODO: das Ergebnis in eine Matrix speichern fuer bessere Ausgabe

NoteNames=['d' 'e' 'f' 'g' 'a' 'h' 'c' 'd' 'e' 'f' 'g'];
NoteNames = ['g' 'f' 'e' 'd' 'c' 'h' 'a' 'g' 'f' 'e' 'd'];

result = [];

for k=1:4
    for i=1:9
        if (LinesYResult(i) - NotesY(k))<=2 && LinesYResult(i) - NotesY(k)>=-2
            result = cat(2, result, NoteNames(i+1));
        end
    end
    if NotesY(k)> (LinesYResult(9)+2)
        result = cat(2, result, NoteNames(11));
    end
    if NotesY(k)+2 < LinesYResult(1)
        result = cat(2, result, NoteNames(1));
    end
end
result

end

function [ resultY ] = ccl(picture)
%% Schritt 5: Connected Component Labeling (CCL)
% Wir verwenden das connected component labeling (CCL) um die
% zusammengehoerigen Komponenten zu finden und so heraus zu finden welche
% Noten vorhanden sind.

% Parameter:
% picture... Bild auf dem die Operation angewendet wird

% Rueckgabe:
% resultY ... Die y-Position der Noten

% Hilfreiche Links:
% http://stackoverflow.com/questions/9067858/labeling-image-with-different-colors
% https://de.mathworks.com/help/images/labeling-and-measuring-objects-in-a-binary-image.html

%bwconncomp: Gibt die verbundenen Komponenten zurueck.
C = bwconncomp(picture);

%labelmatrix: Gibt Matrizen mit Komponenten zurueck
L = labelmatrix(C);
% figure,
% imagesc(L);
% title('CCL');

%Wir suchen uns nun die Koordinaten der Noten (Mitte des Notenkopfs)
maximum = max(L(:)); %Holen uns das Maximum aus der Labelmatrix heraus
resultY = []; %Enthaelt die y-Position der Noten.
for i = 1:maximum
    [x,y] = ind2sub(size(L),find(L==i));
    newy= round(median(x));
    resultY = cat(1, resultY, newy);
end
end

function [ eroded ] = morph(picture, abstandLinien)
%% Schritt 4: Morphologische Operation - Erosion
% Wir verwenden die morphologische Operation (Erosion) um die Linien im
% Bild zu entfernen (Notenlinien & Notenhaelse)

% Parameter:
% picture... das Binarbild auf dem die Operation angewendet wird
% abstandLinien ... der Abstand der Linien. Wir benoetigt um die korrekte
%                   Groesse des Strukturelements zu berechnen

% Rueckgabe:
% eroded ... Dieses Bild enthaelt keine Notenlinien und Notenhaelse mehr

BW = ~im2bw(picture, 0.5); % wir tauschen schwarz mit wei�
se = strel('square', round(abstandLinien/2)); %Strukturelement
eroded = imerode(BW, se); %Anwenden der Erosion

figure,
imshow(eroded);
title('Erosion - Notenlinien entfernen');
end

function[ resultOtsu ] = otsu(picture)
%% Schritt 1: Threshold nach Otsu
% Wir erzeugen ein Binaerbild mit der Methode "Threshold nach Otsu"

% Parameter:
% picture ... das Bild auf dem die Operation angewendet wird

% Rueckgabe:
% resultOtsu ... Das erzeugte Binaerbild

% Hilfreiche Links:
% https://de.mathworks.com/help/images/ref/graythresh.html

[rows, columns numberOfColorChannels] = size(picture);
if numberOfColorChannels > 1
    level = graythresh(picture);
    resultOtsu = im2bw(picture,level); %wir wandeln Bild in Graustufenbild um
else
    resultOtsu = picture; % Das Bild ist bereits grau
end

% figure,
% imshow(resultOtsu);
% title('Threshold nach Otsu');
end

function [ LinesYResult , ROI, abstandLinien] = houghTransformation(picture)
%% Schritt 2b: Hough Transformation
% Wir verwenden die Hough Transformation um die Linien am Notenblatt zu
% erkennen

% Schritt 2b.1: Kantendetektion
% Schritt 2b.2: Hough-Transformation finden
% Schritt 2b.3: Hough-Peaks finden
% Schritt 2b.4: Hough-Lines finden
% Schritt 2b.5: Speichern der x- & y-Werte der Notenlinien
% Schritt 2b.6: Berechnen des Abstandes & die zus�tzlichen Linien abspeichern
% Schritt 2b.7: Region of Interest abspeichern

% Parameter:
% picture ... das Bild auf dem die Operation angewendet wird

% Rueckgabe:
% LinesYResult ... die gesamten Y-Werte der Linien
% ROI ... Region of Interest.
% abstandLinien ... der Abstan der einzelnen Notenlinien

% Hilfreiche Links:
% https://de.mathworks.com/help/images/ref/hough.html
% https://de.mathworks.com/help/images/hough-transform.html#buh9ylp-26
% http://www.stackoverflow.com/questions/9310543/whats-the-use-of-canny-before-houghlines-opencv
% https://de.mathworks.com/help/images/edge-detection.html;

%% Schritt 2b.1: Kantendetektion mit Sobel
resultEdge = edge(picture, 'sobel');
% figure,
% imshow(resultEdge);
% title('Sobel Operator (Kantendetektion)');

%% Schritt 2b.2: Hough-Transformation und diese Anzeigen lassen

% H... Ergebnis der Hough-Transformation
% rho... distance from the origin to the line along
% a vector perpendicular to the line
% theta ...he angle in degrees between the x-axis and this vector
[H,theta,rho] = hough(resultEdge);

%Die Hough-Transformation wird angezeigt
figure,
imshow(imadjust(mat2gray(H)),[],...
    'XData',theta,...
    'YData',rho,...
    'InitialMagnification','fit');
title('Hough Transformation')
%Beschriftung der Achsen
xlabel('\theta (degrees)')
ylabel('\rho')
axis on
axis normal
hold on
colormap(hot)

%% Schritt 2b.3: Peak berechnen und diese Ausgeben lassen.
% Wir berechnen uns die Peaks
peak = houghpeaks(H,5,'threshold',ceil(0.3*max(H(:))));

% Ausgabe der Peaks
x = theta(peak(:,2));
y = rho(peak(:,1));
plot(x,y,'s','color','black');

%% Schritt 2b.4: Hough-Lines
% Linien mit Hilfe der Houghlines-Funktion finden und diese ausgeben lassen.
lines = houghlines(resultEdge,theta,rho,peak,'FillGap',5,'MinLength',7);

yPoints = []; %zum speichern der y-Werte der Notenlinien
xPoints = []; %zum speichern der x-Werte der Notenlinien

% Wir erzeugen einen plot und gibt diesen aus. Das Originalbild wird
% ausgegeben und zusaetzlich werden die Linien im Bild markiert
figure,
imshow(picture),
title('Hough-Transformation - Linien markieren'),
hold on
for k = 1:length(lines)
    xy = [lines(k).point1; lines(k).point2];
    plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
    
    yPoints = [yPoints lines(k).point2(2)]; %wir speichern die y-Werte der Notenlinien
    xPoints = [xPoints lines(k).point2(1)]; %wir speichern die x-Werte der Notenlinien
    
    % Anfangs und Endpunkte werden markiert
    plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
    plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');
end

%% Schritt 2b.5: Speichern der x- & y-Werte
% Speichern der y- & x-Wert der Linien in einem Array und entfernen Duplikate.
linesYPoints = unique(yPoints);
linesXPoints = unique(xPoints);

%% Schritt 2b.6: Berechnen des Abstandes & die zus�tzlichen Linien abspeichern
% Wir speichern uns den Abstand der Linien ab
abstandLinien = abs(linesYPoints(2)-linesYPoints(1));
% Um auch die Noten ueber und unter den Linien zu erhalten, muessen wir uns
% die zus�tzlichen Positionen der Linien berechnen und diese zu den 5
% y-Werten der Linien speichern
extralinie1 = linesYPoints(1)- abstandLinien;
extralinie2 = linesYPoints(end) + abstandLinien;
linesYPoints = cat(2, extralinie1, linesYPoints, extralinie2);
%linesYPoints

%% Schritt 2b.7: Region of Interest
% Wir speichern uns die Position der Linien am Eingabebild ab.
% Dies Position benoetigen wir fuer das Template Matching. Wir suchen nach
% dem Template in diesem Bereich
%ROI = [linesXPoints(1) linesYPoints(1) linesXPoints(end) linesYPoints(end)];
ROI = [1 linesYPoints(1) linesXPoints(end) linesYPoints(end)];
ROI

LinesY = linesYPoints;
LinesYResult = [LinesY(1) LinesY(1)+((LinesY(2)-LinesY(1))/2) LinesY(2) LinesY(2)+((LinesY(3)-LinesY(2))/2) LinesY(3) LinesY(3)+((LinesY(4)-LinesY(3))/2) LinesY(4) LinesY(4)+((LinesY(5)-LinesY(4))/2) LinesY(5)];

end

function [ Loc , matchValue ] = templateMatching(picture, template, ROI)
%% Schritt 3: Template Matching
% Wir suchen ein Template (= bestimmtes Bild) in einem anderen Bild

% Schritt 3.1: Template-Matcher % MarkerInserter
% Schritt 3.2: Anpassung der Groesse des Templates an die Groesse des Bildes
% Schritt 3.3a: Bild in Graustufenbild umwandeln
% Schritt 3.3.b: Template in Graustufenbild umwandeln
% Schritt 3.4: (x,y)-Position des Templates finden
% Schritt 3.5: Uebereinstimmung mit Template ueberpruefen
% Schritt 3.6: Template markieren

% Parameter:
% picture ... Das Bild in dem wir ein Bild suchen
% template ... das Bild das wir suchen
% ROI ... Der Bereich in dem wir das Template suchen

% Rueckgabe:
% Loc ... die x- und y- Position des gefundenen Templates

% Hilfreiche Links:
% https://en.wikipedia.org/wiki/Template_matching
% https://de.mathworks.com/help/vision/ref/vision.templatematcher-class.html
% https://de.mathworks.com/help/vision/ref/templatematching.html
% https://de.mathworks.com/help/images/ref/imresize.html
% https://de.mathworks.com/help/vision/ref/vision.templatematcher-class.html
% https://de.mathworks.com/help/vision/ref/templatematching.html#br80a4_

%% Schritt 3.1: Template-Matcher % MarkerInserter
%Wir verwenden einen TemplateMatcher und eine ROI
htm = vision.TemplateMatcher('ROIInputPort',true);
% Wir verwenden die Methode 'Maximum Absolut Difference' fuer die
% Uebereinstimmung der gefundenen Position im Bild mit dem Template
htm2 = vision.TemplateMatcher('Metric', 'Maximum Absolute Difference');

%Zum Markieren der gefundenen Stelle
hmi = vision.MarkerInserter('Size', 10, ...
    'Fill', true, 'FillColor', 'White', 'Opacity', 0.75);

%% Schritt 3.2: Anpassung der Groesse des Templates an die Groesse des Bildes
% Holen uns die Groesse des Templates
[rowsP columnsP numberOfColorChannelsP] = size(picture);
% Holen uns die Groesse des Templates
[rowsT columnsT numberOfColorChannelsT] = size(template);

% Wir schauen, ob die Bilder gleich hoch sind, wenn nicht, dann resize
if rowsT ~= rowsP
    % Wir berechnen uns das Verhaeltnis, mit dem das Template angepasst
    % wird.
    ratioSize = rowsP/rowsT;
    template = imresize(template, ratioSize);
end

%% Schritt 3.3a: Bild in Graustufenbild umwandeln (mit Threshold nach Otsu)
if numberOfColorChannelsP > 1
   % disp('bild wird in graustufenbild umgewandelt');
    Igray = rgb2gray(picture);
else
    Igray = picture; % It's already gray.
end

%% Schritt 3.3b: Template in Graustufenbild umwandeln (mit Threshold nach Otsu)
if numberOfColorChannelsT > 1
   % disp('template wird in graustufenbild umgewandelt');
    Tgray = rgb2gray(template);
    %Tgray = template;
else
    Tgray = template; % It's already gray.
end

figure,
subplot(2,1,1)
imshow(Igray);
title('Graustufenbild - Notenblatt');

subplot(2,1,2)
imshow(Tgray);
title('Graustufenbild - Template');

%% Schritt 3.4: (x,y)-Position des Templates finden.
% Wir finden hier die Mitte der Position des Templates. Diese benoetigen wir
% um das Template am Bild zu markieren
%Loc = step(htm, Igray, Tgray, ROI);
Loc = step(htm,Igray,Tgray, ROI);
Loc

%% Schritt 3.5: Uebereinstimmung mit Template ueberpruefen
Metric = step(htm2, Igray, Tgray);
%Metric

% Wir berechnen uns, ob das Template vorhanden ist
matchValue = Loc(1)- Metric(1);

%% Schritt 3.6: Template markieren
if (matchValue >= 0) & (matchValue <= 80) %Template vorhanden
    J = step(hmi, Igray, Loc);
    figure,
    imshow(J);
    title('Markiertes Template');
else %Template nicht vorhanden
    figure,
    imshow(Igray);
    title('Template nicht vorhanden');
end

end