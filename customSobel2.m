

function edges = customSobel2(picture)
%% Kantendetection mittels Sobelfilter
%      1 0 -1       1  2  1   Aus dem bin�ren Eingabebild erzeugen wir, mit
% sx = 2 0 -2  sy = 0  0  0   mithilfe der beiden nebenstehenden
%      1 0 -1      -1 -2 -1   Sobelkernel, jeweils einmal in x-Richtung
%                             und einmal in y-Richtung, das gewuenschte
% Gradientenbild nach Pythagoras.

% input:
% picture... bin�res, beschnittenes Eingabebild

% output:
% edges... das fertige Gradientenbild
B=picture;
C=double(B);

for i=1:size(C,1)
    for j=1:size(C,2)
        %Mittelfeld
        if(i > 1 && i < size(C,1) && j > 1 && j < size(C,2))
            Gx = ((C(i+1,j-1)+2*C(i+1,j)+C(i+1,j+1))-(C(i-1,j-1)+2*C(i-1,j)+C(i-1,j+1)));       
            Gy = ((C(i-1,j+1)+2*C(i,j+1)+C(i+1,j+1))-(C(i-1,j-1)+2*C(i,j-1)+C(i+1,j-1)));
            %Bildgradient
            B(i,j)=sqrt(Gx.^2+Gy.^2);
        end
    end
end

%Edge-Strategie: Bildrand ensprechend erweitern
for i=1:size(C,1)
    B(i,1)=B(i,2);
    B(i,size(C,2))= B(i,size(C,2)-1);
end

for j=1:size(C,2)
    B(1,j)=B(2,j);
    B(size(C,1),j)= B(size(C,1)-1,j);
end

edges = B;
end