function lines = customHoughLines(E, theta, rho, peaks) 
    
    lines = repmat(struct('point1', [0, 0], 'point2', [0, 0], 'theta', 0, 'rho', 0), length(peaks), 1 );
    sizeE = size(E);
%     mergeLine = 10;
    for i = 1 : length(peaks)
        currentTheta = double(theta(round(peaks(i,2))));
        currentRho = double(rho(round(peaks(i,1))));
        r1 = (currentRho-double(cosd(currentTheta)))/double(sind(currentTheta));
        r2 = (currentRho-double(sizeE(2)*double(cosd(currentTheta))))/double(sind(currentTheta));
        point1 = [1, r1+1];
        point2 = [sizeE(2), r2+1];
%         point1 = [-1, -1];
%         point2 = [-1, -1]; 
%         start = 0;
%         countDist = 0;
%         for x = 1 : sizeE(1)
%             y = rho-x*cos(theta)/sin(theta);
%             if E(y, x) == 1
%                 if point1(1) == -1
%                     point1 = [y, x];
%                 end
%                 point2 = [y, x];
%             else
%                 countDist = countDist + 1;
%                 if countDist > mergeLine
%              
%         end
        lines(i) = struct('point1', point1, 'point2', point2, 'theta', currentTheta, 'rho', currentRho);
        
    end

end