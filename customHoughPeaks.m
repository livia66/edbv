

function peaks = customHoughPeaks(H, N, tresh, nhood)
%% Finden der Houghpeaks
% Lokalisieren der N maximalen Peaks aus der Hough-Transformationsmatrix H
% unter Ber�cksichtigung der Nachbarschaft nhood und des Schwellwerts
% tresh. Wir iterieren ueber das gesamte Bild und suchen Werte ueber dem
% Schwellwert, welche im potPeaks gespeichert werden. Hat man einen solchen 
% gefunden, pruefen wir die Nachbarschaft rekursiv auf evtl noch groe�ere 
% Werte. Findet man keine groe�eren Werte, so hat man lokal ein Maximum 
% gefunden, wessen Nachbarschaft 0 gesetzt wird. Die N hoechsten Peaks
% werden schliesslich zurueck geliefert.

% input:
% H... Histogramm, Ergebnis der Hough-Transformation
% N... Anzahl der zu findenden peaks
% tresh... Schwellwert, der entscheidet, ob ein peak als peak erkannt wird
% hood... Groe�e der Nachbarschaft

% output:
% peaks... Nx2 Array mit den x/y-Koordinaten der N maximalen peaks

%potPeaks ist maximal so gro�, wie die Summe aller Zahlen in H > tresh 
potPeaks = zeros(sum(sum(H>tresh)),3);
counter = 1;

% Berechnung des Nachbarschaftfeldes. Wird aufgerundet und, falls gerade, 
% +1 addiert
newhood = ceil(sqrt(nhood));
if mod(newhood, 2) == 0
    newhood = newhood + 1;
end

% Iteration ueber H
for i=1:size(H,1)
    for j=1:size(H,2)
        % Ist ein Feld H(i,j) > tresh, so rufen wir checkNhood auf um in
        % der Nachbarschaft evtl groe�ere Werte zu finden.
        if H(i,j) > tresh
            % tmp(1) = x-Koord, tmp(2) = y-Koord, tmp(3) = Wert des Peaks 
            [tmp(1),tmp(2),tmp(3)] = checkNhood(H,i,j,newhood);
            
            % Setzen der Nachbarschaft auf 0 unter Beruecksichtigung der
            % Raender der Matrix
            x1 = max(tmp(1)-fix(newhood/2),1);
            y1 = max(tmp(2)-fix(newhood/2),1);
            x2 = min(size(H,1),tmp(1)+fix(newhood/2));
            y2 = min(size(H,2),tmp(2)+fix(newhood/2));
            
            H(tmp(1),tmp(2)) = 0;
            
            [newi, newj] = get2ndPeak(H(x1:x2,y1:y2));
            newx = x1+newi-1;
            newy = y1+newj-1; 
            
            tmpy = ceil(abs(tmp(1)-newx)/2);
            finaly = min(newx,tmp(1)) + tmpy;
            
            H(x1:x2,y1:y2) = 0;
            
            potPeaks(counter,:) = [finaly tmp(2) tmp(3)];
            counter = counter + 1;
        end
    end 
end
   
   % Die potenziellenPeaks werden sortiert, und die gr��ten N and peaks
   % uebergeben
   potPeaks = sortrows(potPeaks,3);
   peaks = potPeaks(size(potPeaks,1)-N+1:size(potPeaks,1),1:2);  
end

function [x,y,m] = checkNhood(H,i,j,newhood)
%% Ueberpruefen der Nachbarschaft auf groe�ere Werte
% Diese rekursive Hilfsmethode dient dazu, die Nachbarschaft eines Peaks auf
% weitere, hoehere Werte zu untersuchen. Auf diesem hoeherem Wert ruft sich
% checkNhood wiederum erneut auf, solange, bis wir keine hoeheren Peaks mehr
% finden. Die Koordinaten und der Wert des Maximums wird retuniert.

% input:
% H... Histogramm, Ergebnis der Hough-Transformation
% i... x-Koordinate des aktuellen Peaks
% j... x-Koordinate des aktuellen Peaks
% newhood... Groe�e der berechneten Kante einer Nachbarschaft
%            Nachbarschaft = newhood x newhood

% output:
% x... x-Koordinate des evtl neuen, groe�eren Peaks in der Nachbarschaft
% y... y-Koordinate des evtl neuen, groe�eren Peaks in der Nachbarschaft
% m... Wert des evtl neuen, groe�eren Peaks in der Nachbarschaft
%Passe die neighbourhood ensprechend der Randbereiche an
x1 = max(i-fix(newhood/2),1);
y1 = max(j-fix(newhood/2),1);
x2 = min(size(H,1),i+fix(newhood/2));
y2 = min(size(H,2),j+fix(newhood/2));
neighbourhood = H(x1:x2,y1:y2);

% Finde den maximalen Wert in der Neighbourhood
maxval = max(max(neighbourhood));

tmp = [i,j, maxval];

% Falls es einen Wert in der Nachbarschaft gibt, der groe�er ist als der
% aktuelle Wert, finde ihn und f�hre auf ihm rekursive checkNhood aus
if maxval > H(i,j)
    [row, col] = find(neighbourhood == maxval);
    [tmp(1),tmp(2),tmp(3)] = checkNhood(H,row(1)+x1-1,col(1)+y1-1,newhood);       
end

x = tmp(1);
y = tmp(2);
m = tmp(3);
end


function [newi, newj] = get2ndPeak(nhood)
%% Finden des 2nd Peak der lokalen Max-Peak-Neighbourhood
%
maxval = max(max(nhood));
[row, col] = find(nhood == maxval);
newi = row(1);
newj = col(1);
end

