%% Threshold Otsu
% Konvertiert ein Graustufen bzw. RGB Bild zu einem Bin�rbild indem das Bild
% in zwei Klassen unterteilt wird - Hintergrund und Bildobjekte.
% Der Grenzwert l�sst sich durch die maximale Varianz zwischen den Klassen
% finden.

function [ binaryImg ] = thresholdOtsu( image )
    % max: maximale Varianz zwischen den Klassen
    % threshold: gefundener Grenzwert
    max = 0;
    threshold = 0;

    % grayscale image: RGB Bildpunkte werden in L-Grauwerte unterteilt 
    % Grauwerte: [1,2,...,L]
    [rows columns numberOfColorChannels] = size(image);
    if numberOfColorChannels > 1
       grayimg = rgb2gray(image);
    else
       grayimg = image; 
    end

    % histogram: Gibt die Anzahl der Bildpunkte pro Grauwert an. Die 
    % histogram(i) = ni .... Bildpunkte bei Level i 
    % Gesamtanzahl der Bildpunkte: N = n1+n2+...nL = sum(histogram)
    histogram = imhist(grayimg);

    % Wahrscheinlichkeitsverteilung: pi = ni/N    [N...Anzahl der Bildpunkte]
    p = histogram/sum(histogram);
    
    % Grenzwert wird eruiert
    for n = 1:256   % ueber alle Grauwerte iterieren
        % Wahrscheinlichkeit der einzelnen Klassen: pC0 + pC1 = 1
        pC0 = sum(p(1:n));
        pC1 = 1-pC0;
        
        if(pC0 == 0 || pC1 == 0)
           continue; 
        end
        
        % Mittelwert je Klasse: my0 = sum(i*pi/pC0)
        myC0 = sum((1:n)*p(1:n))/pC0;
        myC1 = sum((n+1:256)*p(n+1:256))/pC1;
        
        % Varianz innerhalb und zwischen den Klassen:
        % sigma = pC0*sigma0 + pC1*sigma1
        sigmaC0 = sum((((1:n)-myC0).^2)*p(1:n))/pC0;
        sigmaC1 = sum((((n+1:256)-myC1).^2)*p(n+1:256))/pC1;
        
        % sigma_between/sigma_within = max_value
        % wird benoetigt um den maximalen threshold zu errechnen
        sigmaW = pC0*sigmaC0 + pC1*sigmaC1;
        sigmaB = pC0*pC1*(myC1-myC0)*(myC1-myC0);
        
        sigmaT = sigmaB/sigmaW;
        
        if(sigmaT >= max)
            max = sigmaT;
            threshold = n;
        end
    end
 
    % Klassen werden im Bild neu gesetzt sodass ein Bin�rbild entsteht
    grayimg(grayimg <= threshold) = 0;
    grayimg(grayimg > threshold) = 255;
    
    binaryImg = grayimg;
    
% http://www.gm.fh-koeln.de/~hk/lehre/ala/ws0506/Praktikum/Projekt/F_rot/Bildsegmentierung_Otsu.pdf